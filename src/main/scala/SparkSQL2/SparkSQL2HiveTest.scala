package SparkSQL2

import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, SparkSession}

object SparkSQL2HiveTest {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local")
      .set("spark.sql.warehouse.dir","hdfs://192.168.32.61:9000/user/hive/warehouse")
    val spark = SparkSession.builder()
      .appName(s"${this.getClass.getSimpleName}")
        .enableHiveSupport()
      .config(conf)
      .getOrCreate()

    import spark.implicits._
    import spark.sql

    sql("show databases").show()

  }
}
