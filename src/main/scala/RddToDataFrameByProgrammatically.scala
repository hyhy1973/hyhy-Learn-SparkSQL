import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, Row, SQLContext}

object RddToDataFrameByProgrammatically {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("RddToDataFrameByProgrammatically").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    val rdd = sc.textFile("/Users/hongyi/IdeaProjects/SparkSQLTest/src/main/scala/people.txt")
    //rowRDD:RDD[Row]
    val rowRdd: RDD[Row] = rdd.map(line => Row(line.split(",")(0), line.split(",")(1).trim.toLong))
    //schema:StructType   StructType(list)
    val schema: StructType = StructType(
      StructField("name", StringType, true) ::
        StructField("age", LongType, true) :: Nil
    )


    //(rowRDD,schema)
    val df = sqlContext.createDataFrame(rowRdd, schema)
    df.registerTempTable("people")
    sqlContext.sql("select name,age from people where age>=13 and age<=19").show()
  }
}
