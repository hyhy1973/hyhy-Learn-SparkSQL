import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

object UDFTest {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("DataFrameLoadTest").setMaster("local")
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)

    hiveContext.udf.register("toBig",(name:String)=>{
      if(name !=null){
          name.toUpperCase
      }else{
        "NULL"
      }
    })
  }
}
