import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

object DataFrameTest {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("sqltest")
    val sc = new SparkContext(conf)
    // SparkSQL程序入口
    val sqlContext = new SQLContext(sc)
    // transformation的操作
    val df: DataFrame = sqlContext.read.json("/Users/hongyi/IdeaProjects/SparkSQLTest/src/main/scala/people.txt.json")
    // action 的操作
    // df.show()

    //打印schema信息
    //df.printSchema()

    // select name from people.txt
    //df.select("name").show()

    //df.select(df("name"),df("age")+1).show()

    //select * from people.txt where age>21
    //df.filter(df("age")>21).show()

    //每个年龄多少人
    //df.groupBy(df("age")).count().show()

    //公司里经常应用
    //注册成为一张临时表
    df.registerTempTable("user")
    sqlContext.sql("select name from user where age >=13 and age<=19").show()
  }
}
