import org.apache.spark.sql.hive.HiveContext
import org.apache.spark.{SparkConf, SparkContext}

/*
*
* 窗口函数(开窗函数)
*
*
* 求出每个部门里面工资最高的两个人。。。 分组求topn
* */

object TopN {
  def main(args: Array[String]): Unit = {
       val conf = new SparkConf().setAppName("topn").setMaster("local")
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)


    val sql =
      """
        select * from (select id,salary,bon,dep,row_number() over
        (partition by dep order by salary desc) rank from aura.worker) tmp
        where tmp.rank <=2
      """.stripMargin
    hiveContext.sql(sql).show()

  }
}
