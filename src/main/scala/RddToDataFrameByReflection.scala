import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

object RddToDataFrameByReflection {

  case class Person(name: String, age: Long)

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("RddToDataFrameByReflection").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    //Rdd[object] to DF
    //隐式转换
    import sqlContext.implicits._
    val df: DataFrame = sc.textFile("/Users/hongyi/IdeaProjects/SparkSQLTest/src/main/scala/people.txt")
      .map(x => x.split(","))
      .map(y => Person(y(0), y(1).trim.toLong))
      .toDF()

    df.registerTempTable("people")
    val sql = sqlContext.sql("select name,age from people where age>=13 and age<=19").show()

  }
}
