import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{SQLContext, SaveMode}

object DataFrameLoadTest {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("DataFrameLoadTest").setMaster("local")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)

    //数据源load sparkSQL默认的文件的格式parquet(列式的文件存储格式)文件
    //sqlContext.read.load("url")
    //可以指定一下文件类型
    //sqlContext.read.format("json").load("url")
    //指定存储格式
    //sqlContext.read.load().write.json()
                            //write.jdbc()
                            //write.parquet()
                            //write.save() 使用默认
                            //write.format("json").save()
    //如果存储目录存在
    /*
    * mode(SaveMode.Append) 追加
    * mode(SaveMode.ErrorIfExists) 报错(默认)
    * mode(SaveMode.Overwrite)重写
    * mode(SaveMode.Ignore)不更新
    * */
    //sqlContext.read.load("url").write.mode(SaveMode.ErrorIfExists).format("json").save()


    //数据源之jdbc     使用mysql
    //postgresql类似于mysql关系型数据库  很多公司用他作为hive的元数据库
    sqlContext.read.format("jdbc").options(
      Map("url"->"jdbc:mysql://hadoop4:3306/sparksqltest","dbtable"->"t_1211","user"->"root","password"->"mysql")
    ).load().show()
    //spark-shell --driver-class-path /usr/local/soft/spark/mysql-connector-java-5.1.44-bin.jar
  }
}
