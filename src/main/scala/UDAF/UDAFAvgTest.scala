package UDAF

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.hive.HiveContext

object UDAFAvgTest {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("DataFrameLoadTest").setMaster("local")
    val sc = new SparkContext(conf)
    val hiveContext = new HiveContext(sc)
    hiveContext.udf.register("avg_salary",UDAFTest)
    hiveContext.sql("select avg_salary(salary) from aura.worker").show()
  }
}
