package WebsiteCase

case class ApacheLog(
                    ipAddress:String, // IP地址
                    clientIndent:String,//标识符
                    userId:String,//用户ID
                    dateTime:String, //访问时间
                    method:String,//请求方法
                    endPoint:String,//目标地址
                    protoCal:String,//请求协议
                    responseCode:Int,//状态码
                    contentSize:Long //网页大小
                    )

object ApacheLog{
  // String->ApacheLog对象
  def parseLog(log:String): ApacheLog ={
    val fields = log.split("#")
    val url = fields(4).split(" ")
    ApacheLog(fields(0),fields(1),fields(2),fields(3),url(0),url(1),url(2),fields(5).toInt,fields(6).toLong)
  }
}
